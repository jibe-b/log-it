alias log=". "$(pwd)"/log.sh"

export LOG_FORMAT_VERSION=0.0.1

echo "$1" |sed 's_.*_{"v":"'$LOG_FORMAT_VERSION'","time":"'$(date "+%Y-%m-%dT%H:%M:%S")'", "msg":"&"}_'>>log
(. version-the-l-o-g-s.sh 2>/tmp/err > /tmp/log &)&
